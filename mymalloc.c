/**********************************************************************
  Module: mymalloc.c 
  Author: Scott Wang
  Date:   2005 October 08

  Purpose: malloc and free memory space
**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mymalloc.h"
#include "screen.h"

void *mymalloc(size_t size)
{
    void *p = malloc(size);
    
    if (p == NULL) {
        screen_fini(); 
        fprintf(stderr, "No enough memory to malloc %lu bytes\n", size);
	exit(1);
    }
    return p;
}

void myfree(void *mem)
{
  if(mem == NULL){
   screen_fini();
   fprintf(stderr,"Cannot free NULL pointer\n");
  }
  free(mem);
}
