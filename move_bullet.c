/**********************************************************************
  Module: move_bullet.c 
  Author: Scott Wang
  Date:   2005 October 08

  Purpose: Make bullet goes up each time.
**********************************************************************/

#include <curses.h>
#include <stdlib.h>
#include <assert.h>
#include "move_bullet.h"
#include "screen.h"
#include "bullet.h"
#include "variable.h"

/* keep track bullet's row,col and wether the
   bullet exist on the screen or not */
struct bullet_args {
    int row;
    int col;
    int exist;
};

/*if bullet is out of screen boundary, 
  then remove the bullet */
static int move_bullet_out_screen(Bullet_args bullet);

/* move bullet one row up */
static void move_bullet_up(Bullet_args bullet);



void move_bullet(Dlist d, void *data)
{
  Bullet_args bullet = (Bullet_args) data;
  assert(bullet != NULL);

  screen_draw_image(bullet->row,bullet->col,bullet_str,BULLET_HEIGHT);

  screen_refresh();
  
  screen_clear_image(bullet->row,bullet->col,BULLET_WIDTH,BULLET_HEIGHT);

  move_bullet_up(bullet);
 
  if(move_bullet_out_screen(bullet)){
    dlist_add(d,BULLET_PRE_MOVE,move_bullet,bullet);
  }else{
    remove_a_bullet(bullet);
  }
} 

int move_bullet_out_screen(Bullet_args bullet)
{
  assert(bullet != NULL);
  return (bullet->row > SCR_TOP && bullet->exist);
}

void move_bullet_up(Bullet_args bullet)
{
  assert(bullet != NULL);
 (bullet->row)--;
}

void remove_a_bullet(Bullet_args bullet)
{
    assert(bullet != NULL);
    bullet->exist = FALSE;
}

Bullet_args move_bullet_args_create()
{
 Bullet_args tmp = (Bullet_args) mymalloc(sizeof(struct bullet_args)); 
 return tmp;
}

int is_bullet_existing(Bullet_args bullet)
{
	assert(bullet != NULL);
	return bullet->exist;
}

void set_up_a_bullet(Bullet_args bullet,int row,int col,int exist)
{
	assert(bullet != NULL);
	bullet->row = row;
	bullet->col = col;
	bullet->exist = exist;
}

int get_bullet_row(Bullet_args bullet)
{
	assert(bullet != NULL);
	return bullet->row;
}

int get_bullet_col(Bullet_args bullet)
{
	assert(bullet != NULL);
	return bullet->col;
}
