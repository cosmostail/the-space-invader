/**********************************************************************
  Module: cannon.c
  Author: Jim Uhl
  Date:   2005 October 01

  Purpose: The rows of the cannon image.
**********************************************************************/

#include <assert.h>

#include "cannon.h"

/* The width of each string MUST be CANNON_WIDTH,
   and there MUST be CANNON_HEIGHT elements. */
const char *cannon_str[CANNON_HEIGHT] = {
   " _^_ ",
   "|___|" 
};
