/**********************************************************************
  Module: screen.c
  Author: Jim Uhl
  Date:   2004 October 02
  Modified: 2005 October 01

  Purpose: Initializes curses, draws 2d images on the screen.
**********************************************************************/

#include <string.h>
#include <stdlib.h>
#include <curses.h>

#include "screen.h"

void screen_init(void) {
    initscr();
    crmode();
    noecho();
    clear();

    screen_refresh();
}

void screen_draw_image(int row, int col, const char *image[], int height) {
    int i, length;
    int new_left, new_right, new_offset, new_length;

    for (i = 0; i < height; i++) {
	if (row+i < 0 || row+i >= SCR_HEIGHT)
	    continue;
	length = strlen(image[i]);
	new_left  = col < 0 ? 0 : col;
	new_offset = col < 0 ? -col : 0;
	new_right = col+length > SCR_WIDTH ? SCR_WIDTH-1 : col+length-1;
	new_length = new_right - new_left + 1;

	mvaddnstr(row+i, new_left, image[i]+new_offset, new_length);
    }
}

void screen_clear_image(int row, int col, int width, int height) {
    int i, j;
    
    for (i = 0; i < height; i++) {
	move(row+i, col);
	for (j = 0; j < width; j++)
	    addch(' ');
    }
}

void screen_clear(void) {
    clear();
}

void screen_refresh(void) {
    move(LINES-1, COLS-1);
    refresh();
}

void screen_fini(void) {
    endwin();
}

void screen_check()
{
  if(LINES < SCR_HEIGHT || COLS < SCR_WIDTH){
    screen_fini(); 
    fprintf(stderr,"Screen size too small, minimum should be %d * %d\n",SCR_HEIGHT,SCR_WIDTH);
  exit(1);
  }
}
