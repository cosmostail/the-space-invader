/**********************************************************************
  Module: bullet.c
  Author: Scott Wang
  Date:   2005 October 08

  Purpose: The row of the bullet image 
**********************************************************************/

#include "bullet.h"

/* The width and height of the string must be
   BULLET_WIDTH and BULLET_HEIGHT */
const char *bullet_str[BULLET_HEIGHT] = {
   "+"
};


