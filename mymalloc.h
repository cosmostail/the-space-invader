/**********************************************************************
  Module: mymalloc.h 
  Author: Scott Wang
  Date:   2005 October 08

  Purpose: malloc and free memory space
**********************************************************************/

#ifndef MYMALLOC_H
#define MYMALLOC_H

void *mymalloc(size_t size);
void myfree(void  *d);

#endif
