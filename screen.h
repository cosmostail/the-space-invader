/**********************************************************************
  Module: screen.h
  Author: Jim Uhl
  Date:   2004 October 02
  Modified: 2005 October 01

  Purpose: Draw 2d images on the screen.  Also, draw main game image.
**********************************************************************/

#ifndef SCREEN_H
#define SCREEN_H

/*** Screen limits ***/
#define SCORE_WIDTH 20
#define FIELD_WIDTH (SCR_WIDTH-SCORE_WIDTH)


/* width */
#define SCR_LEFT 0
#define SCR_WIDTH 80
#define SCR_RIGHT (SCR_LEFT+SCR_WIDTH-1)

/* height */
#define SCR_TOP 0
#define SCR_HEIGHT 24
#define SCR_BOTTOM (SCR_TOP+SCR_HEIGHT-1)

/* Initialize curses.  Refreshes screen to terminal. */
extern void screen_init(void);

/* Draws 2d `image' of `height' rows, at curses coordinates `(row, col)'.
   Note: parts of the `image' falling on negative rows are not drawn; each
   row drawn is clipped on the left and right side of the game screen (note
   that `col' may be negative, indicating `image' starts to the left of the
   screen and will thus only be partially drawn.  */
extern void screen_draw_image(int row,
			      int col,
			      const char *image[],
			      int height);

/* Clears a 2d `width'x`height' rectangle with spaces.  Upper left hand
   corner is curses coordinate `(row,col)'. */
extern void screen_clear_image(int row, int col, int width, int height);

/* Clears the screen. */
extern void screen_clear(void);

/* Moves cursor to bottom right corner and refreshes. */
extern void screen_refresh(void);

/* Terminates curses cleanly. */
extern void screen_fini(void);

extern void screen_check();
#endif /* SCREEN_H */
