/**********************************************************************
  Module: move_alien.h
  Author: Scott Wang 
  Date:   2005 October 08

  Purpose:  To make aliens move, and check if aliens are shot or dead
**********************************************************************/

#ifndef MOVE_ALIEN_H
#define MOVE_ALIEN_H

#include "dlist.h"

/*The alien_args is define in move_alien.c*/
typedef struct alien_args *Alien_args;

/* the across number of aliens */
#define ALIEN_NUM_ACROSS 7 

/* the number of aliens kinds */
#define ALIEN_NUM_KINDS (ALIEN_KIND_LAST - ALIEN_KIND_LIVING_FIRST) 

/* The minimum speed aliens can have.If it sets to 0, then
   last alien will move 1 tick pre time */
#define ALIEN_MIN_TICKS_PER_MOVE 0 

/*inittial tick is depend on how many aliens we have plus the 
  minimum speed alien will be.*/
#define ALIEN_INITIAL_TICK_COUNT ((ALIEN_NUM_ACROSS*ALIEN_NUM_KINDS)+ ALIEN_MIN_TICKS_PER_MOVE)

/* move aliens from left side to right side.
   If they touch either left boundary or right 
   boundary,they will move down one row */
extern void move_alien(Dlist d, void *data);

/* check if an alien has been shot */
extern int move_alien_be_shot(Alien_args alien,
                                  int bullet_row, int bullet_col);

/* draw a first image for aliens.
   this function is cp from Jim's 
   invade_src/main.c */
extern void move_alien_first_print();

/* return the number of dead aliens rows*/
extern int getback_dead_rows(Alien_args aliens);

/* create a space for alien argment */
extern Alien_args move_alien_args_create();

/* get back alive aliens number */
extern int getback_alive_aliens_num(Alien_args aliens);

/* get back aliens row number*/
extern int getback_aliens_row(Alien_args aliens);

#endif
