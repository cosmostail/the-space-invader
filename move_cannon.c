/**********************************************************************
  Module: move_cannon.c
  Author: Scott Wang
  Date:   2005 October 08

  Purpose: move cannon from left to right
**********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "move_cannon.h"
#include "screen.h"
#include "cannon.h"
#include "variable.h"
#include "mymalloc.h"

/* keep track cannon's row,col,and direction 
   only move_cannon.c can change those data*/ 
struct cannon_args {
    int row;
    int col;
    int dir;
};

/* if cannon move to screen boundary, then 
   stop move untill player change direction*/
static int move_cannon_out_screen(Cannon_args cannon);

/* print the first cannon image */
static void cannot_move_cannon_further(Cannon_args cannon);

/* clear previous image before move next one */
static void clear_pre_image(Cannon_args cannon);

/*print current alien image*/
static void draw_new_image(Cannon_args cannon);

/*init first position for cannon args*/
static void move_cannon_init_cannon_args(Cannon_args cannon);

void move_cannon(Dlist d, void *data)
{
    Cannon_args cannon = (Cannon_args) data;
    assert(cannon != NULL);
 
    if(move_cannon_out_screen(cannon))
     cannot_move_cannon_further(cannon);
   
    clear_pre_image(cannon);
    draw_new_image(cannon);   
    screen_refresh();
}

int move_cannon_out_screen(Cannon_args cannon)
{
  assert(cannon != NULL);
  return (cannon->col + cannon->dir < SCR_LEFT 
           || cannon->col + cannon->dir > (FIELD_WIDTH-CANNON_WIDTH));
}

void cannot_move_cannon_further(Cannon_args cannon)
{
  assert(cannon != NULL);
  cannon->dir = STOP;
}

void clear_pre_image(Cannon_args cannon)
{
    assert(cannon != NULL);
    screen_clear_image(cannon->row, cannon->col,CANNON_WIDTH,CANNON_HEIGHT);
}

void draw_new_image(Cannon_args cannon)
{
    assert(cannon != NULL);
    screen_draw_image(cannon->row,
                      (cannon->col += cannon->dir),
                      cannon_str,
                      CANNON_HEIGHT);
}

void move_cannon_first_print()
{
     screen_draw_image(SCR_HEIGHT-CANNON_HEIGHT,
                      (FIELD_WIDTH-CANNON_WIDTH)/DIVIDER_SIZE,
                      cannon_str,
                      CANNON_HEIGHT);
}

void move_cannon_init_cannon_args(Cannon_args cannon)
{
  assert(cannon != NULL);
  cannon->row = SCR_HEIGHT-CANNON_HEIGHT;
  cannon->col = (FIELD_WIDTH-CANNON_WIDTH)/DIVIDER_SIZE;
  cannon->dir = STOP;
}

void move_cannon_change_direction(Cannon_args cannon,int dir)
{
 assert(cannon != NULL);
 cannon->dir = dir;
}

int getback_cannon_col(Cannon_args cannon)
{
  assert(cannon != NULL);
  return cannon->col;
}

Cannon_args move_cannon_args_create()
{
 Cannon_args tmp = (Cannon_args) mymalloc(sizeof(struct cannon_args)); 
 move_cannon_init_cannon_args(tmp);
 return tmp;
}

