/**********************************************************************
  Module: alien.h
  Author: Jim Uhl
  Date:   2005 October 01

  Purpose: Generate images of the various aliens in the 'invade' Space
           Invaders variant.

  Notes:

    1. The enumerated type alien_pose_t is a bit awkward - the notions
       of LEFT and RIGHT don't really mean anything.  However, the
       structure does allow for adding additional images of each alien
       easily.
**********************************************************************/

#ifndef ALIEN_H
#define ALIEN_H

/* The possible "poses" for the aliens. */
typedef enum alien_pose {
    ALIEN_POSE_FIRST=0,
    ALIEN_POSE_LEFT=ALIEN_POSE_FIRST,
    ALIEN_POSE_RIGHT,
    ALIEN_POSE_LAST,
} alien_pose_t;

/* The different kinds of alien. */
typedef enum alien_kind {
    ALIEN_KIND_FIRST=0,
    ALIEN_KIND_DEAD=ALIEN_KIND_FIRST, /* A dead alien should draw as blanks. */
    ALIEN_KIND_LIVING_FIRST,	      /* The first of the live alien types. */
    ALIEN_KIND_GENERAL = ALIEN_KIND_LIVING_FIRST,
    ALIEN_KIND_MAJOR,
    ALIEN_KIND_CAPTAIN,
    ALIEN_KIND_SARGEANT,
    ALIEN_KIND_PRIVATE,
    ALIEN_KIND_LAST,
} alien_kind_t;

/* The number of alien types that correspond to living aliens. */
#define ALIEN_KIND_LIVING (ALIEN_KIND_LAST-ALIEN_KIND_LIVING_FIRST)

/* The width of each alien in characters. */
#define ALIEN_WIDTH 3

/* The width of each alien in rows. */
#define ALIEN_HEIGHT 1

/* Return a string representing alien kind k in pose p. */
const char *alien(alien_kind_t k, alien_pose_t p);

#endif /* ALIEN_H */
