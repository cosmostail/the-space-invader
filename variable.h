/**********************************************************************
  Module: variable.h
  Author: Scott Wang
  Date:   2005 October 08

  Purpose: define some variables which will be use in mutiple files
**********************************************************************/


/* default direction for aliens */
#define DIRECTION 1
#define DIR_RIGHT 1
#define BELOW 1

/* default direction for CANNON */
#define STOP 0

/* define for aliens status */
#define DEAD 0

/* define for bullet status */
#define TRUE 1
#define FALSE 0

/* define for default position */
#define DIVIDER_SIZE 2
