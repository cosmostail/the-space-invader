/**********************************************************************
  Module: bullet.h
  Author: Scott Wang
  Date:   2005 October 08

  Purpose: Generate bullet image
**********************************************************************/
#ifndef BULLET_H
#define BULLET_H

#define BULLET_WIDTH 1
#define BULLET_HEIGHT 1

/* Array of string represent bullet image */
const char *bullet_str[BULLET_HEIGHT];

#endif
