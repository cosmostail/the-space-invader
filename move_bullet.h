/**********************************************************************
  Module: move_bullet.h
  Author: Scott Wang
  Date:   2005 October 08

  Purpose: move bullet 
**********************************************************************/


#ifndef MOVE_BULLET_H
#define MOVE_BULLET_H

#include "dlist.h" 

/*bullet_args is a struct which defined in move_bullet.c*/
typedef struct bullet_args *Bullet_args;

/* Bullet speed */
#define BULLET_PRE_MOVE 1 

/*move bullet up one row, check if the bullet
  still exsit on screen*/
extern void move_bullet(Dlist d, void *data);

/* create a space for bullet argment */
extern Bullet_args move_bullet_args_create();

/* check if the bullet is on screen */
extern int is_bullet_existing(Bullet_args bullet);

/*setup a bullet*/
extern void set_up_a_bullet(Bullet_args bullet,int row,int col,int exist);

/* remove a bullet on screen */
extern void remove_a_bullet(Bullet_args bullet);

/*return current bullet row*/
extern int get_bullet_row(Bullet_args bullet);

/*return current bullet co*/
extern int get_bullet_col(Bullet_args bullet);

#endif
