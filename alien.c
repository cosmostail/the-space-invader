/**********************************************************************
  Module: alien.c
  Author: Jim Uhl
  Date:   2005 October 01

  Purpose: Generate images of the various aliens in the 'invade' Space
           Invaders variant.
**********************************************************************/

#include <stdlib.h>
#include <assert.h>
#include "alien.h"

/* TRUE iff k is a legal kind of alien. */
#define ALIEN_KIND_OKAY(k) \
  (ALIEN_KIND_FIRST <= (k) && (k) < ALIEN_KIND_LAST)

/* TRUE iff p is a legal alien pose. */
#define ALIEN_POSE_OKAY(p) \
  (ALIEN_POSE_FIRST <= (p) && (p) < ALIEN_POSE_LAST)

/* Note: width of each pose MUST be ALIEN_WIDTH */
static const char *alien_poses[ALIEN_KIND_LAST][ALIEN_POSE_LAST] = {
    { "   ", "   " },
    { "[O[", "]0]" },
    { "<-<", ">->" },
    { "(=(", ")=)" },
    { "{:{", "}:}" },
    { "/|\\", "\\|/" },
};

const char *alien(alien_kind_t k, alien_pose_t p) {
    assert(ALIEN_KIND_OKAY(k) && ALIEN_POSE_OKAY(p));

    return alien_poses[k][p];
}
