/**********************************************************************
  Module: move_cannon.h
  Author: Scott Wang
  Date:   2005 October 08

  Purpose:move cannon from left to right
**********************************************************************/


#ifndef MOVE_CANNON_H
#define MOVE_CANNON_H

#include "dlist.h"

/*the cannon struct is defined in move_cannon.c
  so outside world doesn't know this struct */
typedef struct cannon_args *Cannon_args;

/* cannon speed */
#define CANNON_PRE_MOVE 2 
 
/* move cannon to left or right*/
extern void move_cannon(Dlist d, void *data);

/*print the first image of the cannon
  This function is cp from Jim's 
  invade_src/main.c
*/
extern void move_cannon_first_print();

/*change the cannon's direction*/
extern void move_cannon_change_direction(Cannon_args cannon,int dir);

/* return back the current col of cannon */
extern int getback_cannon_col(Cannon_args cannon);

/* create a space for cannon argment */
extern Cannon_args move_cannon_args_create();

#endif
