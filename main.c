/**********************************************************************
  Module: main.c
  Author: Scott Wang 
  Date:   2005 October 08

  Purpose: Build a text-base game just like "Space  invade".
**********************************************************************/

#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>

#include "alien.h"
#include "cannon.h"
#include "screen.h"
#include "dlist.h"
#include "move_cannon.h"
#include "move_alien.h"
#include "move_bullet.h"
#include "score.h"
#include "variable.h"
#include "mymalloc.h"

/* define for Command */
#define ESC	'\033'
#define LEFT ','
#define RIGHT '.'
#define BULLET ' ' 

/* define for time out */
#define TUNIT 10000

#define MAX_SCORE 1050

/* define for Score */
#define SCORE_IMAGE "Score "
#define YOU_WIN "You win "
#define YOU_LOST "You lost "

/* finished the game. */ 
void game_fini(Dlist d,Cannon_args c, Bullet_args b, Alien_args a);

/*If any alien soldier enters the row
  of the cannon, the player loses.*/ 
int is_lost(Alien_args aliens);

int main() {
 
    screen_init();
    screen_check();

    move_alien_first_print();
    move_cannon_first_print();
    score_bar_print();
    score_print(SCORE_IMAGE);
    screen_refresh();  

    Dlist head = dlist_create();
   
    int c; 
    fd_set myset;
    struct timeval mytime;
 
    FD_ZERO(&myset);

    Cannon_args cannon_args = move_cannon_args_create();
    Bullet_args bullet_args = move_bullet_args_create();
    Alien_args alien_args = move_alien_args_create();

    dlist_add(head,ALIEN_INITIAL_TICK_COUNT,move_alien,alien_args);
 
    while(1){
     int r;
     FD_SET(fileno(stdin),&myset);
     mytime.tv_sec = 0;
     mytime.tv_usec = TUNIT;
     if((r = select(1,&myset,NULL,NULL,&mytime))==-1){
       perror("select");
       exit(EXIT_FAILURE);
     }

     if(r == 0){ /* time out */
        
      if(dlist_headtick_dec(head) == 0){
  
        if(is_lost(alien_args)){
          screen_clear();
          score_print(YOU_LOST);
          break;
        }
        
	/* update score take a integer as score,
	   move_alien_be_shot return a score if it shot 
	   an alien, otherwise return 0 */
        if(is_bullet_existing(bullet_args)
          && (update_score(move_alien_be_shot(alien_args,
                              		   get_bullet_row(bullet_args),
                                            get_bullet_col(bullet_args))))>0){
          score_print(SCORE_IMAGE);
          remove_a_bullet(bullet_args); 
        }

	/* if there is no more aliens,
   	   then player win */
        if(getback_alive_aliens_num(alien_args) == 0){
          screen_clear();
          score_print(YOU_WIN);
          break;
        }

        dlist_dotick(head);
      }
     }else{
       c = getchar();

       switch (c) {
         case ESC:
                  screen_clear();  
                  game_fini(head,cannon_args,bullet_args,alien_args);
                  return 0; 
         case LEFT:
                  move_cannon_change_direction(cannon_args,-DIRECTION);
                  dlist_add(head,CANNON_PRE_MOVE, move_cannon,cannon_args);
                  break;
         case RIGHT:
                  move_cannon_change_direction(cannon_args,DIRECTION);
                  dlist_add(head,CANNON_PRE_MOVE,move_cannon,cannon_args);
                  break;
         case BULLET:
                  if(!is_bullet_existing(bullet_args)){
                    set_up_a_bullet(bullet_args,SCR_BOTTOM-CANNON_HEIGHT,
                                      (getback_cannon_col(cannon_args)
                                            +CANNON_WIDTH/DIVIDER_SIZE),TRUE);
                                            
                    dlist_add(head,BULLET_PRE_MOVE,move_bullet,bullet_args); 
                  }
                  break;
      }
    }
  }/* end while */
  game_fini(head,cannon_args,bullet_args,alien_args);
  return 0;
}

void game_fini(Dlist d,Cannon_args c, Bullet_args b, Alien_args a)
{
    dlist_free(d);
    myfree(c);
    myfree(b);
    myfree(a);
    screen_refresh();
    screen_fini();
}


int is_lost(Alien_args aliens)
{
 return (getback_aliens_row(aliens) + ALIEN_NUM_KINDS - 
           ALIEN_KIND_LIVING_FIRST - getback_dead_rows(aliens)
            == SCR_BOTTOM - CANNON_HEIGHT); 
}
