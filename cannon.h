/**********************************************************************
  Module: cannon.h
  Author: Jim Uhl
  Date:   2005 October 01

  Purpose: Generate the cannon image.
**********************************************************************/

#ifndef CANNON_H
#define CANNON_H

/* The height and width of the cannon. */
#define CANNON_WIDTH 5
#define CANNON_HEIGHT 2

/* Array of strings representing the cannon. */
const char *cannon_str[CANNON_HEIGHT];

#endif /* CANNON_H */
