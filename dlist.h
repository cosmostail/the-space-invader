/**********************************************************************
  Module: dlist.h
  Author: Jim Uhl
  Date:   2004 October 02

  Purpose: Provide multiple software timer capability.  The idea is that
    the user has several events they want to schedule to run at future
    times, specified by a number of 'ticks'.  Each event has a tick 
    count, indicating how far in the future to run the event, a function
    pointer, doit, that contains the action to be performed, and
    a data pointer passed into the doit function when the function is
    run.  In addition, the doit function gets the scheduler list
    (known as a "delta list") so that it can schedule future events if
    necessary.
**********************************************************************/
#ifndef DLIST_H
#define DLIST_H

/* The delta list data type (note that it is defined in dlist.c - the
   outside world does _not_ know its structure.*/
typedef struct dlist *Dlist;

/* An `action' is a functon pointer taking a delta list and an arbitrary
   pointer - this allows you to pass in a pointer to anything you
   like.*/

typedef void (*action)(Dlist, void *); 

/* Create a new, empty, delta list. */
extern Dlist dlist_create(void);

/* Add a new event to `d'.  Event will run `ticks' time units from now, and
   execute `doit' with parameter `data' at that time.

   Note: `ticks' must be > 0.
*/
extern void dlist_add(Dlist d, int ticks, action doit, void *data);

/* Run all events scheduled for the next tick, and remove them from `d'. */
extern void dlist_dotick(Dlist d);

/* Returns iff `d' is empty. */
extern int dlist_empty(Dlist d);

/* Finalize `d', freeing up any memory. */
extern void dlist_free(Dlist d);

/* The function will be called in main.c, each
   time the caller will get the decrement value
   of the head tick */
extern int dlist_headtick_dec(Dlist d);

#endif /* DLIST_H */
