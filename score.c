/**********************************************************************
  Module: score.c
  Author: Scott Wang
  Date:   2005 October 08

  Purpose:print a score bar and score on the screen
**********************************************************************/

#include "score.h"
#include "screen.h"
#include <curses.h>
#include "variable.h"

#define MAX_NUM_SIZE 128
#define SCORE_BAR_IMAGE "|"
#define SCORE_BAR_WIDTH 1

static int score = 0;

void score_bar_print()
{
 int i;
 for (i = 0; i <= SCR_BOTTOM; i++){ 
  mvaddstr(i,FIELD_WIDTH,SCORE_BAR_IMAGE);
 }
 screen_refresh();
}

void score_print(char *str)
{
  char buf[MAX_NUM_SIZE];
  snprintf(buf,(sizeof(buf)+1),"%d",score);
 
  mvaddstr((SCR_TOP+SCR_BOTTOM)/DIVIDER_SIZE,FIELD_WIDTH+SCORE_BAR_WIDTH,str);
  mvaddstr((SCR_TOP+SCR_BOTTOM)/DIVIDER_SIZE,FIELD_WIDTH
                                        +SCORE_BAR_WIDTH+(sizeof(str)+1),buf); 
 
  screen_refresh();
}

int update_score(int s)
{
	score += s;
	return s;
}