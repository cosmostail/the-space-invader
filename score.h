/**********************************************************************
  Module: score.h
  Author: Scott Wang
  Date:   2005 October 08

  Purpose: print a score bar and score on the screen
**********************************************************************/


#ifndef SCORE_H
#define SCORE_H

/* print score bar*/
extern void score_bar_print();

/* take a string and a score to print on screen
   for example, str = "you win ", score = 100, then
   print "you win 100"*/
extern void score_print(char *str);

/* update score after an alien been killed */
extern int update_score(int score);
#endif
