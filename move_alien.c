/**********************************************************************
  Module: move_alien.c
  Author: Scott Wang
  Date:   2005 October 08

  Purpose: move_alien is a modules that impelemnt all aliens' movement
           and record how many aliens dead. 
**********************************************************************/
#include <curses.h>
#include <stdlib.h>
#include <assert.h>
#include "move_alien.h"
#include "alien.h"
#include "screen.h"
#include "mymalloc.h"
#include "variable.h"

/* define for score */
#define ALIEN_GENERAL_SCORE 50
#define ALIEN_PRIVATE_SCORE 10

/* define for aliens if they move down  */
#define ALIENS_MOVE_DOWN 1

/* the spaces beteen aliens */
#define ALIEN_SEPARATION 3 

/* array for alive aliens */
static int alien_alive[ALIEN_KIND_LAST][ALIEN_NUM_ACROSS]; 


struct alien_args {
	int row;
	int col;
	int dir;
	/* counter how many aliens are still alive*/
	int alive_count;
	/* to calculate how many bounding rows have been shot*/
	int right_col_dead;
	int left_col_dead;
	int bottom_row_dead;
};

/*if aliens move out of the boundary, return true*/
static int move_alien_out_of_bound(Alien_args aliens);

/* clear last image on screen before draw a new image*/
static void move_alien_clear_pre_image(Alien_args aliens);

/* draw a new image of aliens */
static void move_alien_draw_image(Alien_args aliens);

/* if is not possible to shoot an alien, return true*/
static int not_possible_to_shoot_an_alien(int alien_row,int alien_col
					,int bullet_row,int bullet_col);

/*check if bullet is in right or left side of aliens */
static int bullet_is_in_right_or_left_side_of_aliens(int alien_col,
							int bullet_col);
							 
/* check if bullet is above or below of aliens */
static int bullet_is_in_above_or_below_of_aliens(int alien_row, 
						 int bullet_row);
						 
/*check if bullet is in the spaces between aliens */
static int bullet_is_in_alien_separation_area(int alien_col,int bullet_col);

/*check if the alien below the current alien is dead, if isn't, don't kill */
static int the_alien_below_is_dead(int alien_alive_index_i,
					 int alien_alive_index_j);

/*check if left most side of aliens are dead */
static void check_if_left_most_aliens_are_dead(Alien_args aliens);

/* check if right most side of aliens are dead */
static void check_if_right_most_aliens_are_dead(Alien_args aliens);

/*to check if a bottom of aliens are dead*/
static void check_if_bottom_aliens_are_dead(Alien_args aliens);

/*initial aliens argsment, setup direction, default col, and row etc*/
static void init_alien_args(Alien_args aliens);

void move_alien(Dlist d, void *data)
{
   Alien_args aliens = (Alien_args)data;
   assert(aliens != NULL);
    
   move_alien_clear_pre_image(aliens);    
   
   if(move_alien_out_of_bound(aliens)){
     aliens->dir = - aliens->dir;
     aliens->row += ALIENS_MOVE_DOWN;
   }
   aliens->col += aliens->dir;
   move_alien_draw_image(aliens);
    
  dlist_add(d,aliens->alive_count+ALIEN_MIN_TICKS_PER_MOVE,move_alien,aliens);
}

int move_alien_be_shot(Alien_args aliens,int bullet_row, int bullet_col)
{
  assert(aliens != NULL);
  
  int score = 0;
  if(not_possible_to_shoot_an_alien(aliens->row,aliens->col,
                                       bullet_row,bullet_col))

  return score;
 
  int alien_alive_index_i = bullet_row-aliens->row;
  int alien_alive_index_j =
      (bullet_col-aliens->col)/(ALIEN_WIDTH+ALIEN_SEPARATION);

  if(alien_alive[alien_alive_index_i][alien_alive_index_j] != DEAD
    && the_alien_below_is_dead(alien_alive_index_i,alien_alive_index_j)){
  
      score = alien_alive[alien_alive_index_i][alien_alive_index_j];
      alien_alive[alien_alive_index_i][alien_alive_index_j] = DEAD;
      aliens->alive_count--;
  }
 
  return score;
}

int bullet_is_in_right_or_left_side_of_aliens(int alien_col,int bullet_col)
{
 return
     (bullet_col < alien_col
      || bullet_col
         > alien_col
	 +(ALIEN_WIDTH+ALIEN_SEPARATION)*ALIEN_NUM_ACROSS - ALIEN_SEPARATION);
}

int bullet_is_in_above_or_below_of_aliens(int alien_row, int bullet_row)
{
 return (bullet_row  > alien_row + ALIEN_NUM_KINDS || bullet_row < alien_row); 
}

int bullet_is_in_alien_separation_area(int alien_col,int bullet_col)
{
 return ALIEN_WIDTH <=((bullet_col-alien_col)%(ALIEN_WIDTH+ALIEN_SEPARATION));
}

int the_alien_below_is_dead(int i, int j)
{
  return (i == ALIEN_NUM_KINDS || alien_alive[i+BELOW][j] == DEAD);
}
 
int not_possible_to_shoot_an_alien(int alien_row,int alien_col,
                                          int bullet_row,int bullet_col)
{
 return  (bullet_is_in_above_or_below_of_aliens(alien_row,bullet_row)   
          || bullet_is_in_right_or_left_side_of_aliens(alien_col,bullet_col)
          || bullet_is_in_alien_separation_area(alien_col,bullet_col));
}

/* cp from jim's invade_src */
void move_alien_first_print()
{
 alien_kind_t i;
    int j;
  for (i = ALIEN_KIND_LIVING_FIRST; i < ALIEN_KIND_LAST; i++) {
        for (j = 0; j < ALIEN_NUM_ACROSS; j++) {
            const char *a = alien(i,
                                  ALIEN_POSE_FIRST
                                  + j % (ALIEN_POSE_LAST-ALIEN_POSE_FIRST));
            screen_draw_image((int)i+SCR_TOP,
                              j*(ALIEN_WIDTH+ALIEN_SEPARATION),
                              &a,
                              ALIEN_HEIGHT);
            alien_alive[(int)i][j] =
		(ALIEN_GENERAL_SCORE + ALIEN_PRIVATE_SCORE)
		- (int)i*(ALIEN_PRIVATE_SCORE);
        }
  }
}

void move_alien_clear_pre_image(Alien_args aliens)
{
    assert(aliens != NULL);
	
    int k,col;
    for (k = 0; k < ALIEN_NUM_ACROSS-aliens->right_col_dead; k++) {
            col = k*(ALIEN_WIDTH+ALIEN_SEPARATION)+ aliens->col;
            screen_clear_image(aliens->row, col ,ALIEN_WIDTH,
            ALIEN_HEIGHT * 
              (ALIEN_KIND_LAST-ALIEN_KIND_FIRST-aliens->bottom_row_dead));
    }
    screen_refresh();
}

int move_alien_out_of_bound(Alien_args aliens)
{
   assert(aliens != NULL);
   
   int col;
   if(aliens->dir == DIR_RIGHT ){
     col = (ALIEN_NUM_ACROSS-aliens->right_col_dead)
		 *(ALIEN_WIDTH+ALIEN_SEPARATION)+aliens->col-ALIEN_SEPARATION;
   }else{
     col = aliens->col +
               aliens->left_col_dead * (ALIEN_WIDTH+ALIEN_SEPARATION);
   }  

   return (col <= SCR_LEFT || col >= FIELD_WIDTH);
}

static int popo = 1;
void move_alien_draw_image(Alien_args aliens)
{
    assert(aliens != NULL);
    
    alien_kind_t i;
    int j;
    int alien_pose_i,alien_pose_j;
    
    for (i = ALIEN_KIND_LIVING_FIRST; 
               i < ALIEN_KIND_LAST-aliens->bottom_row_dead; i++) {
        for (j = 0; j < ALIEN_NUM_ACROSS - aliens->right_col_dead; j++) {

         if(alien_alive[i][j] == DEAD){
           alien_pose_i = alien_pose_j = DEAD;
         }else{
           alien_pose_i = i;
           alien_pose_j = (++popo)%2;  
         }

         const char *a = alien(alien_pose_i,alien_pose_j);

         screen_draw_image((int)i+SCR_TOP+aliens->row,
                             (j*(ALIEN_WIDTH+ALIEN_SEPARATION))+(aliens->col),
                              &a,
                              ALIEN_HEIGHT);
        }
    }
    check_if_left_most_aliens_are_dead(aliens);
    check_if_right_most_aliens_are_dead(aliens);
    check_if_bottom_aliens_are_dead(aliens);
    screen_refresh(); 
}

void check_if_left_most_aliens_are_dead(Alien_args aliens)
{
  assert(aliens != NULL);
  
  aliens->left_col_dead = 0;
  while(alien_alive[ALIEN_KIND_LIVING_FIRST][aliens->left_col_dead] == DEAD)
    aliens->left_col_dead++;
}

void check_if_right_most_aliens_are_dead(Alien_args aliens)
{
  assert(aliens != NULL);
  
  aliens->right_col_dead = 0;
  while(alien_alive[ALIEN_KIND_LIVING_FIRST]
            [ALIEN_NUM_ACROSS-1-aliens->right_col_dead] == DEAD)
     aliens->right_col_dead++;
}

void check_if_bottom_aliens_are_dead(Alien_args aliens)
{
	assert(aliens != NULL);
	
	int j = 0;
	aliens->bottom_row_dead = 0;
	while(alien_alive[ALIEN_NUM_KINDS-aliens->bottom_row_dead][j] == DEAD){
		j++;
		if(j == ALIEN_NUM_ACROSS){
		  aliens->bottom_row_dead++;
		  j = 0;
		}
	}
}

int getback_dead_rows(Alien_args aliens)
{
 assert(aliens != NULL);
 return aliens->bottom_row_dead;
}

Alien_args move_alien_args_create()
{
 Alien_args tmp = (Alien_args) mymalloc(sizeof(struct alien_args)); 
 init_alien_args(tmp);
 return tmp;
}

void init_alien_args(Alien_args aliens)
{
	assert(aliens != NULL);
	
	aliens->row = SCR_TOP;
	aliens->col = SCR_LEFT;
	aliens->dir = DIR_RIGHT;
	aliens->right_col_dead = 0;
	aliens->left_col_dead = 0;
	aliens->bottom_row_dead = 0; 
	aliens->alive_count = ALIEN_NUM_ACROSS * ALIEN_NUM_KINDS;
}

int getback_alive_aliens_num(Alien_args aliens)
{
	assert(aliens != NULL);
	return aliens->alive_count;
}

int getback_aliens_row(Alien_args aliens)
{
	assert(aliens != NULL);
	return aliens->row;
}
