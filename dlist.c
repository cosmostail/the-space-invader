/**********************************************************************
  Module: dlist.c 
  Author: Scott Wang
  Date:   2005 October 08

  Purpose: Implementation of delta-list with double link-list
**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "mymalloc.h"
#include "dlist.h"
#include <assert.h>


/*  strcut dlist contains 
  "tick" is used for keeping each event time.
  "data" contains the arguments of function point action event.
  "event" is a function point to point the actully action. */
struct dlist{
   int tick;
   void *data;
   action event;
   struct dlist *next;
   struct dlist *pre;
};

/* The function is used only in dlist.c module.
   If head is empty, then create a space */ 
static void dlist_insert_if_empty(Dlist d,int ticks, action doit, void *data);

/* If head isn't NULL, and the head tick is greater than the ticks we
   pass in, then insert the node at front of list */
static void dlist_insert_at_front(Dlist d,int ticks, action doit, void *data);

/* if we are in the tail of the list, insert at back*/
static void dlist_insert_at_back(Dlist d,int ticks, action doit, void *data);

/* create a temp space, update all the points, and insert into the
   middle of list*/
static void dlist_insert_at_middle(Dlist d,int ticks, action doit, void *data);


Dlist dlist_create(void)
{
 Dlist tmp = (Dlist ) mymalloc(sizeof(struct dlist));
 return tmp;
}

void dlist_insert_if_empty(Dlist d, int ticks, action doit, void *data)
{
    assert(d != NULL);

    d->next = dlist_create();
    d->next->next = NULL;
    d->next->pre = d;
    d->next->tick = ticks;
    d->next->event  = doit;
    d->next->data = data;
}

void dlist_insert_at_front(Dlist d, int ticks, action doit, void *data)
{
    assert(d != NULL);

    d->next->pre = dlist_create();
    d->next->pre->tick = ticks;
    d->next->pre->event = doit;
    d->next->pre->data = data;
    d->next->pre->next = d->next;
    d->next->pre->pre = d;
    d->next = d->next->pre;
    d->next->next->tick -= ticks; 
}

void dlist_insert_at_back(Dlist d, int ticks, action doit, void *data)
{ 
      assert(d != NULL);   
  
      d->next = dlist_create();
      d->next->next = NULL;
      d->next->pre = d;
      d->next->tick = ticks - d->tick;
      d->next->event = doit;
      d->next->data = data;
}

void dlist_insert_at_middle(Dlist d, int ticks, action doit, void *data)
{
  assert(d != NULL); 
 
  Dlist tmp = dlist_create();
  tmp->pre = d->pre;
  tmp->next = d;
  d->pre->next = tmp;
  d->pre = tmp;
  tmp->tick = ticks;
  tmp->next->tick -= tmp->tick;
  tmp->event = doit;
  tmp->data = data;
}
 
void dlist_add(Dlist d, int ticks, action doit, void *data)
{
  assert(ticks > 0);
 
  if(dlist_empty(d->next)){
    dlist_insert_if_empty(d,ticks,doit,data);
    return;
  }


  if(d->next->tick > ticks){
    dlist_insert_at_front(d,ticks,doit,data);
    return;
  }
 
  d = d->next;
 /* find a good position, if it is tail, add
    a node to back */
  while(d->tick < ticks){
    if(d->next == NULL){
      dlist_insert_at_back(d,ticks,doit,data);
      return;
    }
    ticks -= d->tick;
    d = d->next;
  }

  dlist_insert_at_middle(d,ticks,doit,data);
  return;
}

void dlist_dotick(Dlist d)
{
  assert(d != NULL);

  Dlist tmp = d->next;
  while( tmp != NULL && tmp->tick == 0){
    tmp->event(d,tmp->data);
    tmp->pre->next = tmp->next;
    tmp->next->pre = tmp->pre;
    myfree(tmp);
    tmp = d->next;
  }
}

int dlist_empty(Dlist d)
{
  return (d == NULL);
}

void dlist_free(Dlist d)
{
 Dlist tmp;
 while(d != NULL){
  tmp = d;
  d = d->next;
  myfree(tmp);
 }
}

/* decrement head ticks. Since in main, we 
   create a empty head, therefore, the 
   actually head is head->next */
int dlist_headtick_dec(Dlist d)
{
 assert(d != NULL);

 d = d->next;
 return --(d->tick);
}
