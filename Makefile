######################################################################
#  File:   Makefile
#  Author: Scott Wang
#  Date:   2004 October 05
#  Modified: 2005 October 01
#
#  Purpose: Build instructions for sample code to exercise screen
#           drawing routines.
#
######################################################################

LDLIBS=-lcurses
CFLAGS=-g -Wall
LDFLAGS=-g

EXE=invade

OBJS=main.o screen.o cannon.o alien.o bullet.o mymalloc.o dlist.o move_cannon.o move_alien.o move_bullet.o score.o

$(EXE): $(OBJS)
	gcc $(LDFLAGS) -o $(EXE) $(OBJS) $(LDLIBS)

move_cannon.o: move_cannon.c move_cannon.h dlist.h screen.h cannon.h variable.h mymalloc.h

move_alien.o: move_alien.c move_alien.h dlist.h screen.h alien.h variable.h

alien.o: alien.c alien.h

cannon.o: cannon.c cannon.h

main.o: main.c alien.h cannon.h screen.h dlist.h mymalloc.o move_cannon.h move_alien.h move_bullet.h score.h variable.h

screen.o: screen.c screen.h

dlist.o: dlist.c dlist.h mymalloc.h

mymalloc.o: mymalloc.c mymalloc.h screen.h

move_bullet.o: move_bullet.c move_bullet.h dlist.h screen.h cannon.h variable.h 

score.o: score.c score.h screen.h

bullet.o: bullet.c bullet.h

clean:
	rm -f core $(EXE) $(OBJS)
